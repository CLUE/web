/*
 *  person_schema.sql
 *
 *  author: Jed S. Baer
 *
 *  A SQL script for creating the CLUE website database for
 *  keeping track of people and sponsors
 *
 */

create table person (
    id                  smallint
        not null auto_increment primary key,
    last_name           varchar(40) not null,
    first_name          varchar(40) not null,
    initial             char(1),
    bio                 text
);

create table officer (
    office              varchar(20)
        not null primary key,
    person_id           smallint,
    username            varchar(32) not null unique key,
    password            varchar(32) not null
);

create table member (
    id                  smallint
        not null auto_increment primary key,
    login_name          varchar(32) not null unique key,
    person_id           smallint not null unique key,
    date_join           date,
    paid_through        date,
    home_directory      varchar(80)
);

create table sponsor (
    id                  smallint
        not null auto_increment primary key,
    name                varchar(80) not null,
    expiration_date     date,
    premier             bool not null,
    logo                varchar(255),
    blurb               text
);

create table contact (
    id                  smallint
        not null auto_increment primary key,
    person_id           smallint,
    sponsor_id          smallint,
    type                varchar(12) not null,
    contact             text not null,
    publish             bool not null
);
