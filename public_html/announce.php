<div class="block">
<a name="announcements"></a><h3>Announcements</h3>
<div class="content">
<p>
<b>CLUE Website:</b> If you find errors in the web site please report them to
<?php echo WEBMASTER; ?>.  The site is hosted on gitorious so you can help
improve it (starting with the <a href="display.php?node=sitedevelopment">CLUE
Dev</a> page, perhaps.
<p>
<b>CLUE Presentations:</b> If you would like to make a presentation at a CLUE meeting, or know someone who would, please contact our Speaker Coordinator: <?php echo SPEAKERS; ?>.
<p>
<b>CLUE Sponsorship:</b> If your organization or business would like to become a <a href="display.php?node=sponsors">CLUE Sponsor</a>, please contact <?php echo SPONSORS; ?>.
<p>
<b>CLUE Donations:</b> CLUE now has a donation button. David Willson is serving as our agent for accepting donations to CLUE's treasury via Paypal. If you would care to donate money in support of CLUE, please use the <i>Donate</i> button in the sidebar. CLUE funds are used to purchase web hosting services, and supplies for Installfests.
<p>
</div>
</div>
