<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

 <title><?php echo $pagetitle; ?></title>
 <meta name="AUTHOR" content="CLUE">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="KEYWORDS"
  content="Colorado Linux User Group Open Source Free Software"
 >
 <meta name="DESCRIPTION"
  content="<?php echo $pagedesc; ?>"
 >
 <meta name="ROBOTS" content="ALL">
 <base target="_self">

 <link rel="stylesheet" type="text/css"
       href="main.css"
 >

</head>
<body>

<?php include ('top_nav.php'); ?>

<table class="main" WIDTH=100% cols=2 border=0 cellpadding=0 cellspacing=0>
<tr>
  <td class="leftnav" width=160>
    <?php
        include ('activities.html');
        include ('dtc_sidebar.html');
        // include ('north_sidebar.html');
        include ('linux.html');
        include ('donate.html');
    ?>
  </td>
  <td>
    <div class=main>
