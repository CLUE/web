\version "2.8.5"
\header {
	title = "I've Got Lilycode"
 subtitle = "A Compositional Example of Lilycode Notation"
 subsubtitle= "Lilypond 2.8.5-1"
}
%Definitions:

% ***************************************************************
% Style / Tempo Definition 
% ****************************************************************
tempoMark =
{
	\once \override Score.RehearsalMark #'extra-offset = #'(-1.0 . 2.0)
	\mark \markup { \small \bold { Swing } }
	\once \override Score.MetronomeMark #'extra-offset = #'(0.0 . 2.3)
	\tempo 4= 132
}

% ****************************************************************
% Segno Mark overide  Definition 
% ****************************************************************
segnoMark =
{
	\once \override Score.RehearsalMark #'self-alignment-X = #right
	\mark \markup { \musicglyph #"scripts.segno" }
}

% ****************************************************************
% Fine Mark Overide Definition 
% ****************************************************************
fineMark =
{
	\once \override Score.RehearsalMark #'break-visibility = #begin-of-line-invisible	\once \override Score.RehearsalMark #'self-alignment-X = #right \mark "Fine  "
}

% ****************************************************************
% Rehearsal Marks Definitions
% ****************************************************************
boxedA = \mark \markup { \bold \box { A } }
boxedB = \mark \markup { \bold \box { B } }
segnoAndBoxedA = \mark \markup
{
	\override #'(baseline-skip . 5)
	\column
	{
		\musicglyph #"scripts.segno"
		\bold \box { A }
	}
}

% ****************************************************************
% "D.S. al" Definition
% ****************************************************************
desegnoMark =
{
	\once \override Score.RehearsalMark #'break-visibility = #begin-of-line-invisible	\once \override Score.RehearsalMark #'self-alignment-X = #right \mark \markup { \small \bold "D.S. al Coda"}
}

% ****************************************************************
% "Coda Sign" Definition
% ****************************************************************
 codaMark =
{
        \mark \markup { \musicglyph #"scripts.coda" }
}

% ****************************************************************
% ****************************************************************
% Begining of Actual Notation
% ****************************************************************
% ****************************************************************
melody = \relative c'
{
	\clef treble
	\key d \major
% Creates actual 4/4 time as opposed to "Common" time ("C")
        \override Staff.TimeSignature #'style = #'()
        \time 4/4
        \autoBeamOff
        \set Score.skipBars = ##t



% ****************************************************************
% Beginning of the actual notation code
% ***************************************************************
\tempoMark
% Intro - 12 bar rest - Line 1
       R1*8 | \bar "||:" \break

% Melody - Letter "A" - Measure 9
%Beginning of 1st % 2nd Ending repeat.  The notation is placed inside of {}

      \repeat volta 2
{
        a'8[ fis8]
% Position Override of Rehearsal Letter "A"
% \segnoAndBoxedA
 \once \override Score.RehearsalMark #'extra-offset = #'(-08.5 . 0.0)  \segnoAndBoxedA
        d4-^ c'4. c8 |
        b8[ g8 e8 ais8->]( ais4) r8 ais8 |
        a?8[ fis8] d4-^ c'8[ a8] fis4-^ |
        b8[ g8 e8 ais8] r4 r8 a8 |\break

%Melody - Line 3, Measure 13
       fis8[ g8 a8 fis8] e8[ d8] r4 |
 \mark \markup { \musicglyph #"scripts.coda" } 
       g8[ d8 g8 gis8->]( gis4) r8 b8 |
}

% 1st Ending & 2nd Ending placed in {} with each individual ending in its own {}
\alternative
       {
% 1st Ending
             { a8[ g8 fis8 e8] dis8[ c'8 b8 a8] |
               g4-^ e8[d8] cis8[ a'8] r4 | \break }
% 2nd Ending - Line 4 - Measure 17
             { a8[ g8 fis8 g8] fis8[ d8 b8 d8->]( |
             d2) r2 | \bar "||" \break }
       }


%Melody - Line 5, Measure 19 - Letter B
\once \override Score.RehearsalMark #'extra-offset = #'(-2.0 . 1.5)
      \mark \markup {\bold \box {B}}
      fis2 fis8[ fis8] r4 |
      ais4-- b8[ c8] cis8[ fis,8] r8 fis8 | 
      b4-^ r8 b8 a8[ b8 cis8 a8] |
      gis8[ a8 b8 fis8] r2 | \break

      e2 e8[ e8] r4 |
      gis4-- a8[ ais8] b8[ e,8] r8 e8| 

%Melody - Line 6, Measure 23
      a4-^ r8 a8 g8[ a8 b8 g8] |
      fis8[ g8 a8^\markup \bold "D.S. al Coda"  e8] r2
% \once \override Score.RehearsalMark #'extra-offset = #'(-9.0 . 1.5)
| \bar "||" \break

%Coda - Line 7 - Measure 23
\once \override TextScript #'extra-offset = #'(-10.0 . 2.5)
      g8^\markup \bold { CODA } [ d8 \once \override Score.RehearsalMark #'extra-offset = #'(-9.5 . 1.5) \codaMark  g8 gis8->]( gis4) r8 b'8 | 
      a8[ g8 fis8 g8] fis8[ d8 b8 d8->]( |
      d2)  r2^\markup \bold "Fine" |  \bar "|."

 }


%**************************************************
     harmonies = \chordmode

 {
% Chords - Line 1
      r1*8   |   d2:maj7 b:m7 | e:m7 e:7 | fis:m7 b:m7 | e:m7 a:7 | d1:7
% Chords - Line 2, Measure 13
      g:7 | fis2:m7 b:m7 | e:m7 a:7 | e:m7 a:7 | d1:maj7 |
% Chords - Line 3 - Letter B
      fis:7 | s1 | b:7 | s1 | e:7 | s1 |
% Chords - Line 4
      a:7 | s1 |
% Chords - Coda 
      g:7 | e2:m7 a:7 | d1:maj7 |

 }


\score
{
%	\transpose g d
	<<
		\new ChordNames \harmonies
        \new Staff \melody
	>>

	\layout
	{
		\context
		{
			\Score
			skipBars = ##t
			autoBeaming = ##f
			\override MultiMeasureRest #'expand-limit = #1		    \override TimeSignature #'style = #'()
			\override Hairpin #'after-line-breaking = ##t
		}
		\context
		{
			\Staff
			voltaOnThisStaff = ##f
		}
		\context
		{
			\ChordNames
			chordChanges = ##t
			\consists "Volta_engraver" 
			voltaOnThisStaff = ##t
			\override VoltaBracket #'extra-offset = #'(0 . -3)
		}
%       ragged-last = ##t 
	}

	\midi { \tempo 4=96 }
}


