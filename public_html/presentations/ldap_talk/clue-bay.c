#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ldap.h>
#include <lber.h>

#define  MY_LDAP_HOST  "ldap.baylor.edu"
#define  MY_LDAP_PORT  389
#define  MY_LDAP_DN    NULL
#define  MY_LDAP_PW    NULL

#define	MY_LDAP_SEARCHBASE	"o=Baylor University,c=US"
#define	SCOPE			LDAP_SCOPE_SUBTREE
#define	FILTER			"(&(mail=*)(cn=leigh*))"

int main( int argc, char **arv ) {
	LDAP		*ld;
	int		rc, msgID, i, rc2;
	LDAPMessage	*result;
	LDAPMessage	*e;
	char		*attribs[10];
	BerElement	*ber;
	char		*attribute;
	char		**vals;
	struct timeval	timeout;
	int		finished;

	timeout.tv_sec = 30L;
	timeout.tv_usec = 0L;

	if( ( ld = ldap_init( MY_LDAP_HOST, LDAP_PORT ) ) == NULL ) {
		perror( "ldap_init" );
	}
	printf( "connected to LDAP host %s on port %d\n", MY_LDAP_HOST, LDAP_PORT );

	/* Bind to the server */
	printf( "binding...\n" );
	msgID = ldap_simple_bind( ld, MY_LDAP_DN, MY_LDAP_PW );
	printf( "msgID=%d\n", msgID );

	rc = 0;
	finished = 0;
	while( ! finished ) {
		rc = ldap_result( ld, msgID, LDAP_MSG_ONE, &timeout, &result );
		printf( "rc=%d\n", rc );
		if( rc == -1 ) {
			finished = 1;
			rc2 = ldap_result2error( ld, result, 1 );
			printf( "ldap_result: %d : %s\n", rc2, ldap_err2string( rc2 ) );
			ldap_unbind( ld );
			exit( 1 );
		}
		if( rc >= 0 ) {
			finished = 1;
			rc2 = ldap_result2error( ld, result, 1 );
			printf( "rc2=%d\n", rc2 );
			if( rc2 != LDAP_SUCCESS ) {
				printf( "ldap_simple_bind: %d : %d : %s\n", rc, rc2, ldap_err2string( rc ) );
				ldap_unbind( ld );
				exit( 1 );
			}
			else {
				printf( "Bind Successful\n" );
			}
		}
	}

	/* Search for some stuff */
	attribs[0] = "cn";
	attribs[1] = "mail";
	attribs[2] = "givenName";
	attribs[3] = "sn";
	attribs[4] = "uid";
	attribs[5] = NULL;

	printf( "searching...\n" );
	rc = ldap_search_ext_s( ld, MY_LDAP_SEARCHBASE, SCOPE, FILTER, attribs,
		0, NULL, NULL, &timeout, 0, &result );
	if( rc == LDAP_TIMEOUT ) {
		printf( "LDAP Timeout Error: ldap_search_st: %d : %s\n", rc, ldap_err2string( rc ) );
	  	ldap_unbind( ld );
		exit( 1 );  
	}
	if( rc != LDAP_SUCCESS ) {
		printf( "LDAP Error: ldap_search_st: %d : %s\n", rc, ldap_err2string( rc ) );
	  	ldap_unbind( ld );
		exit( 1 );  
	}

	printf( "Total results are: %d\n", ldap_count_entries( ld, result ) );
	for( e = ldap_first_entry( ld, result ); e != NULL; e = ldap_next_entry( ld, e ) ) {
		printf( "DN: %s\n", ldap_get_dn( ld, e ) );

		/* Addtributes and values */
		for( attribute = ldap_first_attribute( ld, e, &ber );
			attribute != NULL;
			attribute = ldap_next_attribute( ld, e, ber ) )
		{
			if( ( vals = ldap_get_values( ld, e, attribute ) ) != NULL ) {
				for( i = 0; vals[i] != NULL; i++ ) {
					printf( "\t%s: %s\n", attribute, vals[i] );
				}
			}
			/* Free memory used to store values */
			ldap_value_free( vals );
		}

		/* Free memory used to store attribute */
		ldap_memfree( attribute );

		if( ber != NULL ) {
			ber_free( ber, 0 );
		}
		printf( "\n" );
	}
	
	ldap_msgfree( result );
	printf( "Search success!\n" );

	ldap_unbind( ld );

	exit( 1 );  

}

/* End of Source. */

    
