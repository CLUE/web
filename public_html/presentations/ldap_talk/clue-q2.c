#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ldap.h>
#include "lber.h"

#define MY_LDAP_HOST        "localhost"
#define MY_LDAP_PORT        389
#define MY_LDAP_DN          NULL
#define MY_LDAP_PW    	    NULL

#define	MY_LDAP_SEARCHBASE  "dc=matchg, dc=com"
#define	SCOPE               LDAP_SCOPE_SUBTREE
#define	FILTER              "(&(mail=*)(|(givenName=m*)(sn=g*)))"

int main( int argc, char **argv ) {
	LDAP		*ld;
	LDAPMessage	*result;
	LDAPMessage	*e;
	BerElement	*ber;
	char		*attribs[3];
	char		*attribute;
	char		**vals;
	int		rc, i;

	/* Connect to server */
	if( ( ld = ldap_init( MY_LDAP_HOST, LDAP_PORT ) ) == NULL ) {
		perror( "ldap_init" );
	}
	printf( "connected to LDAP host %s on port %d\n", MY_LDAP_HOST, LDAP_PORT );

	/* Search for some stuff */
	attribs[0] = NULL;
	rc = ldap_search_ext_s( ld, MY_LDAP_SEARCHBASE, SCOPE, FILTER, attribs, 0,
		NULL, NULL, LDAP_NO_LIMIT, 0, &result );
	if( rc != LDAP_SUCCESS ) {
		printf( "LDAP Error: ldap_search_st: %s\n", ldap_err2string( rc ) );
		ldap_unbind( ld );
		exit( 1 );  
	}
	printf( "Total results are: %d\n", ldap_count_entries( ld, result ) );

	/* Process results */
	for( e = ldap_first_entry( ld, result ); e != NULL; e = ldap_next_entry( ld, e ) ) {
		printf( "DN: %s\n", ldap_get_dn( ld, e ) );

		/* Attributes and values */
		for(	attribute = ldap_first_attribute( ld, e, &ber );
			attribute != NULL;
			attribute = ldap_next_attribute( ld, e, ber ) )
		{
			if( ( vals = ldap_get_values( ld, e, attribute ) ) != NULL ) {
				for( i = 0; vals[i] != NULL; i++ ) {
					printf( "\t%s: %s\n", attribute, vals[i] );
				}
			}
			/* Free memory used to store values */
			ldap_value_free( vals );
		}

		/* Free memory used to store attribute */
		ldap_memfree( attribute );

		if( ber != NULL ) {
			ber_free( ber, 0 );
		}

		printf( "\n" );
	}
	ldap_msgfree( result );
	printf( "Search success!\n" );
	ldap_unbind( ld );
	exit( 1 );  
}
