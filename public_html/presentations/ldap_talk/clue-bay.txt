connected to LDAP host ldap.baylor.edu on port 389
binding...
msgID=1
rc=97
rc2=0
Bind Successful
searching...
Total results are: 25
DN: cn=Leigh Roberts, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Katherine Roberts
	cn: Leigh Roberts
	mail: Leigh_Roberts@baylor.edu
	givenName: Leigh Katherine
	sn: Roberts
	uid: Leigh_Roberts

DN: cn=Leigh Robertson, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Robertson
	mail: Leigh_Robertson@baylor.edu
	givenName: Leigh
	sn: Robertson
	uid: Leigh_Robertson

DN: cn=Leigh Perry, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Ann Perry
	cn: Leigh Perry
	mail: Leigh_Perry@baylor.edu
	givenName: Leigh Ann
	sn: Perry
	uid: Leigh_Perry

DN: cn=Leigh Tidwell, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Ann Tidwell
	cn: Leigh Tidwell
	mail: Leigh_Tidwell@baylor.edu
	givenName: Leigh Ann
	sn: Tidwell
	uid: Leigh_Tidwell

DN: cn=Leigh Ann Marshall, ou="Engineering & Computer Science Dean's Office - Eng/Comp Sci", ou=Faculty and Staff, ou=People, o=Baylor University, c=US
	cn: Leigh Ann Marshall
	cn: Leigh A. Marshall
	mail: Leigh_Ann_Marshall@baylor.edu
	givenName: Leigh Ann
	givenName: Leigh A.
	sn: Marshall
	uid: Leigh_Ann_Marshall

DN: cn=Leigh Stanley, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Patrese Stanley
	cn: Leigh Stanley
	mail: Leigh_Stanley@baylor.edu
	givenName: Leigh Patrese
	sn: Stanley
	uid: Leigh_Stanley

DN: cn=Leigh Goforth, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Tucker Goforth
	cn: Leigh Goforth
	mail: Leigh_Goforth@baylor.edu
	givenName: Leigh Tucker
	sn: Goforth
	uid: Leigh_Goforth

DN: cn=Leigh Cunningham, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Erin Cunningham
	cn: Leigh Cunningham
	mail: Leigh_Cunningham@baylor.edu
	givenName: Leigh Erin
	sn: Cunningham
	uid: Leigh_Cunningham

DN: cn=Leigh Berry, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Ann Berry
	cn: Leigh Berry
	mail: Leigh_Berry@baylor.edu
	givenName: Leigh Ann
	sn: Berry
	uid: Leigh_Berry

DN: cn=Leigh-Taylor Hester, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh-Taylor Hester
	mail: Leigh-Taylor_Hester@baylor.edu
	givenName: Leigh-Taylor
	sn: Hester
	uid: Leigh-Taylor_Hester

DN: cn=Joseph White, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leighton White
	cn: Joseph Leighton White
	cn: Joseph White
	mail: Joseph_White@baylor.edu
	givenName: Leighton
	givenName: Joseph Leighton
	sn: White
	uid: Joseph_White

DN: cn=Leigh Livesay, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Ellen Livesay
	cn: Leigh Livesay
	mail: Leigh_Livesay@baylor.edu
	givenName: Leigh Ellen
	sn: Livesay
	uid: Leigh_Livesay

DN: cn=Leighanne King, ou=Other, ou=People, o=Baylor University, c=US
	cn: LeighAnne K. Fischer
	cn: Leighanne King
	mail: Leighanne_King@baylor.edu
	givenName: LeighAnne K.
	sn: Fischer
	uid: Leighanne_King

DN: cn=Leigh Janke, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Janke
	cn: Cathryn Leigh Janke
	mail: Leigh_Janke@baylor.edu
	givenName: Leigh
	givenName: Cathryn Leigh
	sn: Janke
	uid: Leigh_Janke

DN: cn=Leigh Prichard, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Prichard
	mail: Leigh_Prichard@baylor.edu
	givenName: Leigh
	sn: Prichard
	uid: Leigh_Prichard

DN: cn=Holly Brown, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leighanne Holly Brown
	cn: Holly Brown
	mail: Holly_Brown@baylor.edu
	givenName: Leighanne Holly
	sn: Brown
	uid: Holly_Brown

DN: cn=Leigh Symank, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Symank
	cn: Leigh Anne Symank
	mail: Leigh_Symank@baylor.edu
	givenName: Leigh
	givenName: Leigh Anne
	sn: Symank
	uid: Leigh_Symank

DN: cn=Leigh Bull, ou=Unknown Classification, ou=Student, ou=People, o=Baylor University, c=US
	cn: Leigh Ann Bull
	cn: Leigh A Bull
	cn: Leigh Bull
	mail: Leigh_Bull@baylor.edu
	givenName: Leigh Ann
	givenName: Leigh A
	sn: Bull
	uid: Leigh_Bull

DN: cn=Leigh Craine, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Barksdale Craine
	cn: Leigh Craine
	mail: Leigh_Craine@baylor.edu
	givenName: Leigh Barksdale
	sn: Craine
	uid: Leigh_Craine

DN: cn=Leigh Mitchell, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Andra Mitchell
	cn: Leigh Mitchell
	mail: Leigh_Mitchell@baylor.edu
	givenName: Leigh Andra
	sn: Mitchell
	uid: Leigh_Mitchell

DN: cn=Leighanne Woodlee, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leighanne Alise Woodlee
	cn: Leighanne Woodlee
	mail: Leighanne_Woodlee@baylor.edu
	givenName: Leighanne Alise
	sn: Woodlee
	uid: Leighanne_Woodlee

DN: cn=Niki Sutton, ou=Unknown Classification, ou=Student, ou=People, o=Baylor University, c=US
	cn: Niki Sutton
	cn: Leigh Nicole Sutton
	mail: Niki_Sutton@baylor.edu
	givenName: Niki
	givenName: Leigh Nicole
	sn: Sutton
	uid: Niki_Sutton

DN: cn=Leighanne Harper, ou=Other, ou=People, o=Baylor University, c=US
	cn: LeighAnne Harper
	cn: Christa LeighAnne Harper
	mail: Leighanne_Harper@baylor.edu
	givenName: LeighAnne
	givenName: Christa LeighAnne
	sn: Harper
	uid: Leighanne_Harper

DN: cn=Leigh Ann Moffett, ou=Safety Services - Fin&Adm, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh Ann Moffett
	cn: Leigh A. Moffett
	mail: Leigh_Ann_Moffett@baylor.edu
	givenName: Leigh Ann
	givenName: Leigh A.
	sn: Moffett
	uid: Leigh_Ann_Moffett

DN: cn=Leigh-Ann Mulsow, ou=Other, ou=People, o=Baylor University, c=US
	cn: Leigh-Ann Mulsow
	mail: Leigh-Ann_Mulsow@baylor.edu
	givenName: Leigh-Ann
	sn: Mulsow
	uid: Leigh-Ann_Mulsow

Search success!
