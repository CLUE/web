#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ldap.h>

#define	MY_LDAP_HOST        "localhost"
#define	MY_LDAP_PORT        389
#define MY_LDAP_USER        "cn=manager, dc=matchg, dc=com"
#define MY_LDAP_PW          "secret"

#define MY_LDAP_ENTRY_DN    "uid=donaldd, dc=matchg, dc=com"

int main( int argc, char **arv ) {
	LDAP    *ld;
	int     rc;
	LDAPMod *mods[2];
	LDAPMod attrib;
	char    *val_em[] = { "don@quack.com", "duck@disney.com", NULL };

	if( ( ld = ldap_init( MY_LDAP_HOST, LDAP_PORT ) ) == NULL ) {
		perror( "ldap_init" );
	}
	printf( "connected to LDAP host %s on port %d\n", MY_LDAP_HOST, LDAP_PORT );

	/* Bind to the server */
	rc = ldap_simple_bind_s( ld, MY_LDAP_USER, MY_LDAP_PW );
	printf( "rc=%d\n", rc );
	if( rc != LDAP_SUCCESS ) {
		printf( "LDAP Error: ldap_simple_bind_s: %s\n", ldap_err2string( rc ) );
		ldap_unbind( ld );
		exit( 1 );
	}

	/* Setup attributes to modify */
	attrib.mod_op = LDAP_MOD_REPLACE;
	attrib.mod_type = "mail";
	attrib.mod_values = val_em;

	mods[0] = &attrib;
	mods[1] = NULL;	

	/* Modify mail entry */
	rc = ldap_modify_ext_s( ld, MY_LDAP_ENTRY_DN, mods, NULL, NULL );
	printf( "rc=%d\n", rc );
	if( rc != LDAP_SUCCESS ) {
		printf( "LDAP Error: ldap_modify_ext_s: %s\n", ldap_err2string( rc ) );
		ldap_unbind( ld );
		exit( 1 );  
	}
	printf( "Update successful!\n" );

	ldap_unbind( ld );
	exit( 1 );  
}
