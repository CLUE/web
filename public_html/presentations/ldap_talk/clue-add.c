#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ldap.h>

#define	MY_LDAP_HOST        "localhost"
#define	MY_LDAP_PORT        389
#define MY_LDAP_USER        "cn=manager, dc=matchg, dc=com"
#define MY_LDAP_PW          "secret"

#define MY_LDAP_ENTRY_DN    "uid=donaldd, dc=matchg, dc=com"

int main( int argc, char **arv ) {
	LDAP    *ld;
	int     rc;
	LDAPMod *mods[6];
	LDAPMod attrib1, attrib2, attrib3, attrib4, attrib5;
	char    *val_cn[] = { "Quack", NULL };
	char    *val_sn[] = { "Duck", NULL };
	char    *val_gn[] = { "Donald", NULL };
	char    *val_oc[] = { "person", "inetOrgPerson", NULL };
	char    *val_em[] = { "donald@duck.com", "quack@zoo.com", NULL };

	if( ( ld = ldap_init( MY_LDAP_HOST, LDAP_PORT ) ) == NULL ) {
		perror( "ldap_init" );
	}
	printf( "connected to LDAP host %s on port %d\n", MY_LDAP_HOST, LDAP_PORT );

	/* Bind to the server */
	rc = ldap_simple_bind_s( ld, MY_LDAP_USER, MY_LDAP_PW );
	printf( "rc=%d\n", rc );
	if( rc != LDAP_SUCCESS ) {
		printf( "LDAP Error: ldap_simple_bind_s: %s\n", ldap_err2string( rc ) );
		ldap_unbind( ld );
		exit( 1 );
	}

	/* Create entry */
	attrib1.mod_op = LDAP_MOD_ADD;
	attrib1.mod_type = "cn";
	attrib1.mod_values = val_cn;

	attrib2.mod_op = LDAP_MOD_ADD;
	attrib2.mod_type = "sn";
	attrib2.mod_values = val_sn;

	attrib3.mod_op = LDAP_MOD_ADD;
	attrib3.mod_type = "givenName";
	attrib3.mod_values = val_gn;

	attrib4.mod_op = LDAP_MOD_ADD;
	attrib4.mod_type = "objectClass";
	attrib4.mod_values = val_oc;

	attrib5.mod_op = LDAP_MOD_ADD;
	attrib5.mod_type = "mail";
	attrib5.mod_values = val_em;

	mods[0] = &attrib1;
	mods[1] = &attrib2;
	mods[2] = &attrib3;
	mods[3] = &attrib4;
	mods[4] = &attrib5;
	mods[5] = NULL;	

	/* Add new entry with attributes */
	rc = ldap_add_ext_s( ld, MY_LDAP_ENTRY_DN, mods, NULL, NULL );
	printf( "rc=%d\n", rc );
	if( rc != LDAP_SUCCESS ) {
		printf( "LDAP Error: ldap_add_ext_s: %s\n", ldap_err2string( rc ) );
		ldap_unbind( ld );
		exit( 1 );  
	}
	printf( "Add successful!\n" );

	ldap_unbind( ld );
	exit( 1 );  
}
