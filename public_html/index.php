<?php

(file_exists('../config/local_config.php'))? require ('../config/local_config.php') : require('../config/config.php');

require('./defines.php');

$pagetitle = 'Colorado Linux Users and Enthusiasts';
$pagedesc = 'The website of the Colorado Linux Users and Enthusiasts, a Linux Users Group for the Denver Colorado Metropolitan Area';

include('common.php');
// for "top importance announcement, uncomment the following line
include('top_announce.html');
include('splash.html');

//  main.php will be incorporated later, when the whiz-bang database stuff
//  is in place
// include('main.php');

// commenting out the following line, as it returns an error -- JSB; 6-Dec-09
// include('cluepresentationlist.html');
include('DTC_meeting.html');
include('web_site.html');
// include('north_meeting.html');
include('installfest.html');
include('announce.php');
include('search.html');

include('footer.php');
?>
