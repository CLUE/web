US DOMAIN TEMPLATE

US Version:  Jul 97

PLEASE DO NOT ALTER THIS APPLICATION IN ANY WAY.  Send your
.US registration to hostmaster@westnet.net.  See attached detailed
instructions.

*****************************************************************

      1.   REGISTRATION TYPE
           (N)ew (M)odify (D)elete..: M

      2.   FULLY-QUALIFIED DOMAIN NAME: clue.denver.co.us

      3.   ORGANIZATION INFORMATION
      3a.  Organization Name.....: Colorado Linux Users & Enthusiasts (CLUE)
      3b.  Address Line 1........: 9046 E Nassau Ave
      3b.  Address Line 2........: 
      3c.  City..................: Denver
      3d.  State.................: CO
      3e.  Zip/Code..............: 80237-1917

      4.   DESCRIPTION OF ORG/DOMAIN: 
 
CLUE is a Denver based Linux users group which hosts
mailing lists and monthly meetings to discuss open source 
software, programming languages and other topics related
to the GNU/Linux computer operating system.

      5.   Date Operational......: (?) 1995 (?)

      6.   ADMINISTRATIVE CONTACT OF ORG/DOMAIN
      6a.  NIChandle (if known)..:
      6b.  Whole Name............: Lynn Danielson
      6c.  Organization Name.....: Colorado Linux Users & Enthusiasts (CLUE)
      6d.  Address Line 1........: 9046 E Nassau Ave
      6d.  Address Line 2........: 
      6e.  City..................: Denver
      6f.  State.................: CO
      6g.  Zip/Code..............: 80237-1917
      6h.  Voice Phone...........: (303)771-7299
      6i.  Electronic Mailbox....: lynnd@techangle.com

      7.   TECHNICAL AND ZONE CONTACT
      7a.  NIChandle (if known)..: HI403-ORG
      7b.  Whole Name............: 
      7c.  Organization Name.....: 
      7d.  Address Line 1........: 
      7d.  Address Line 2........:
      7e.  City..................: 
      7f.  State.................: 
      7g.  Zip/Code..............: 
      7h.  Voice Phone...........: 
      7i.  Electronic Mailbox....: 
      7j.  Registration Mailbox..:
      7k.  Fax Number............: 

      8.   PRIMARY SERVER: HOSTNAME, NETADDRESS
      8j.  Hostname..............: DNS0.TECHANGLE.COM
      8k.  IP Address............: 199.239.19.9


      9.   SECONDARY SERVER: HOSTNAME, NETADDRESS
      9a.  Hostname..............: CORELIA.TECHANGLE.COM
      9b.  IP Address............: 216.87.68.125


