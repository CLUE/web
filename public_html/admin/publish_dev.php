<?php

//  take the name of the CVS tag as input
//  feed it to the shell script to publish
//  the development website

if (isset($_POST['self']) && $_POST['self'] == $PHP_SELF) {

?>
<html><title>Publishing site</title><body><pre>
<?php

    $cvstag = escapeshellarg($_POST['cvstag']);
    $pubcmd = sprintf('/var/www/cgi-bin/publish_dev.sh %s',$cvstag);

//  the following non-compact code is written because I have suspicions
//  about the way the system function works. the docs say it returns
//  FALSE if the command fails, but I believe this based on an assumption
//  of no output on stdout if the command fails -- a null string is
//  cast as boolean FALSE. 'if (system($pubcmd))' isn't a reliable indicator.

    system($pubcmd,$retv);
    if ($retv == 0) {
        echo '<p>Publish complete.';
    } else {
        echo 'Unable to publish using that tag.';
    }

?></pre></body></html>
<?php

} else {

?>
<html>
<title>Publish development site</title>
<body>
<h3>Publish the CLUE development website</h3>
<form action="publish_dev.php" method="POST">
<b>Enter CVS tag for release: 
<input type="text" name="cvstag" size="40" maxlength="40">
<p>
<input type="hidden" name="self" value="<?php echo $PHP_SELF;?>">
<input name="submit" value="publish" type="submit">
</form>
<hr noshade>
<a href="http://cluedenver.org/">Back to CLUE</a>
</body>
</html>
<?php
}
