<?php

//  invoke the production website publishing script

?>
<html><title>Publishing site</title><body>
<?php

    $pubcmd = '/usr/local/www/cgi-bin/publish.sh';

//  the following non-compact code is written because I have suspicions
//  about the way the system function works. the docs say it returns
//  FALSE if the command fails, but I believe this based on an assumption
//  of no output on stdout if the command fails -- a null string is
//  cast as boolean FALSE. 'if (system($pubcmd))' isn't a reliable indicator.

?>
<b>Publishing the CLUE Website</b><p>Please wait ...<p><pre>
<?php
    system($pubcmd,$retv);
?>
</pre>
Unless there were fatal errors, <a href="http://cluedenver.org/">take a look</a>
</body></html>
