<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

 <title>CLUE Admin Page</title>
 <meta name="AUTHOR" content="Jed S. Baer">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="DESCRIPTION" content="CLUE Admin Page">
 <meta name="ROBOTS" content="none">

 <base target="_self">

 <link rel="stylesheet" type="text/css"
       href="../default.css"
 >

</head>
<body>
<h3>CLUE Admin Page</h3>
<div style="background-color: white; color: black;">
<center>
<table BORDER=0 CELLSPACING=10 CELLPADDING=10 WIDTH="600" BGCOLOR="#FFFFFF" NOSAVE >
<tr NOSAVE>
<td NOSAVE>
<DIV ALIGN="CENTER"><H1>CLUE Domain Registration Information</H1>
<hr NOSHADE WIDTH="100%">
</DIV>
<table BORDER=0 WIDTH="100%" BGCOLOR="#FFFFFF" NOSAVE >
<tr NOSAVE>
<td NOSAVE>
</td>

<td>
<p>Below is the "us" domain template needed for updating the
clue.denver.co.us and associated domains.  It is filled in the
way it was last submitted.  Other domains which we currently use
include <a href="linux.denver.co.us">linux.denver.co.us</a> and <a href="lug.denver.co.us">lug.denver.co.us</a>.

<p>The point of contact for denver.co.us subdomains can be found
<a href="http://www.us/register/delegated_subdomains.txt">here</a>.
As of Nov '02 it is:
<a href="mailto:us-dom-adm@westnet.net">us-dom-adm@westnet.net</a>

<p>The real person that has been answering requests is
<a href="mailto:cgarner@ieee.org">Chris Garner</a>.

<p>The following can be retreived as a plain text file (Unix line enders)
<a href="domain.txt">here</a>.

<pre>
<?php include ('domain.txt'); ?>
</pre>

</td>
</tr>
</table>
</div>
</body></html>
