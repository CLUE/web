<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

 <title>CLUE Admin Page</title>
 <meta name="AUTHOR" content="Jed S. Baer">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="DESCRIPTION" content="CLUE Admin Page">
 <meta name="ROBOTS" content="none">

 <base target="_self">

 <link rel="stylesheet" type="text/css"
       href="../default.css"
 >

 <style type="text/css">
 <!--
    b {color: black;}
    li {color: black;}
 -->
 </style>

</head>
<body>
<h3>CLUE Admin Page</h3>
<hr width=100% noshade>
<div style="background-color: white; margin: 0.5em; padding; 0.5em; border: 1px solid black;">
<ul>
<li><a href="contacts.php">Admin Users</a></li>
<li><a href="domain.php">Domain Registration</a></li>
<li><a href="/usage/index.html">Web Statistics</a></li>
<li><a href="publish.php">Publish the CLUE Website</a></li>
<li><b>CLUE Admin Policies and Information</b></li>
  <ul>
    <li><a href="mirki/meeting_policies.html">General Meeting Policies</a></li>
    <li><a href="mirki/vpslink.html">VPSLink</a></li>
    <li><a href="mirki/webupdate.html">Website Updates</a></li>
    <li><a href="mirki/mailadmin.html">Mail Admin</a></li>
    <li><a href="mirki/cluejobs.html">CLUE Jobs</a></li>
    <li><a href="mirki/sysadmin.html">System Administration</a></li>
    <li><a href="mirki/netadmin.html">Domain Administration</a></li>
    <li><a href="mirki/rsync.html">Rsync Over SSH</a></li>
  </ul>
</li>
</ul>
</div></body></html>
