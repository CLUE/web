<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

 <title>CLUE Admin Page</title>
 <meta name="AUTHOR" content="CLUE">
 <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="DESCRIPTION" content="CLUE Admin Page">
 <meta name="ROBOTS" content="none">

 <base target="_self">

 <link rel="stylesheet" type="text/css"
       href="../default.css"
 >

</head>
<body>
<h3>CLUE Contacts Page</h3>
<TABLE BORDER="1" CELLSPACING="0" CELLPADDING="2" NOSAVE BORDERCOLOR="#000000"
 style="text-align: left; width: 100%;">
    <tr>
      <td style="vertical-align: top; font-weight: bold;">Name
      </td>
      <td style="vertical-align: top; font-weight: bold;">Primary Email
      </td>
      <td style="vertical-align: top; font-weight: bold;">Alternate Email
      </td>
      <td style="vertical-align: top; font-weight: bold;">Primary Phone
      </td>
      <td style="vertical-align: top; font-weight: bold;">Alternate Phone
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Dennis J. Perkins
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
      <td style="vertical-align: top;">&nbsp;
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Dave Anselmi
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">&nbsp;
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Jeff Cann
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Lynn Danielson
      </td>
      <td style="vertical-align: top;">&nbsp;
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Collins Richey
      </td>
      <td style="vertical-align: top;">someone@example.com
      </td>
      <td style="vertical-align: top;">&nbsp;
      </td>
      <td style="vertical-align: top;">800-555-1212
      </td>
      <td style="vertical-align: top;">&nbsp;
      </td>
    </tr>
    <tr><td colspan=5><b>Generic e-mail addresses</b></td></tr>
    <tr>
      <td style="vertical-align: top;">President
      </td>
      <td style="vertical-align: top;">president@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">North
      </td>
      <td style="vertical-align: top;">north@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Sponsors
      </td>
      <td style="vertical-align: top;">sponsors@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Speakers
      </td>
      <td style="vertical-align: top;">speakers@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Membership
      </td>
      <td style="vertical-align: top;">membership@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Events
      </td>
      <td style="vertical-align: top;">events@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Webmaster
      </td>
      <td style="vertical-align: top;">thewebmasterguy@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Reviews
      </td>
      <td style="vertical-align: top;">reviews@cluedenver.org
      </td>
      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>


</table>
</body></html>
