<?php

/*
 *  meeting.php
 *
 *  an object class for manipulating CLUE meeting records
 *
 *  auther: Jed S. Baer
 *
 */

class meeting {

    var     $date;
    var     $location;
    var     $presentation;      // array of presentation data,
                                // or free-form event listing
    var     $counter;

    var     $type_display = array ('kiss' => '<b>KISS Session: </b>', 'main' => '<b>Main Topic: </b>');

//  internal utility functions

    function timecomp($t1, $t2) {

        $t1 = strtotime($t1['time']);
        $t2 = strtotime($t2['time']);
        return strcmp($t1, $t2);

    }


//  constructor function

    function meeting() {
  
        $this->counter = 0;
  
    }

//  accessor functions; we pretend these attributes are private

    function set_date($date) {

    // perform some data validation here? or, since this isn't
    // really private, hold validation until the insert?

        $this->date = $date;
    }

    function set_location($location) {
        $this->location = $location;
    }

    function set_presentation($time, $link) {

        $this->presentation[$this->counter]['time'] = $time;
        $this->presentation[$this->counter]['link'] = $link;

        $this->counter++;
    }

    function set_special_event($time,$event) {

        $this->presentation[$this->counter]['time'] = $time;
        $this->presentation[$this->counter]['special_event'] = $event;

        $this->counter++;
    }

    function element_start($parser, $element, $attribute) {

        switch ($element) {
            case 'meeting':
              $this->location = $attribute['location'];
              $this->curr_tag = '';
              break;
            case 'presentation':
              $this->presentation[$this->counter]['type'] = $attribute['type'];
              $this->curr_tag = '';
              break;
            case 'special_event':
              $this->presentation[$this->counter]['type'] = 'special';
              $this->para_counter = 0;
              $this->curr_tag = '';
              break;
            default:
              $this->curr_tag = $element;
        }

    }

    function element_end($parser, $element) {

        // when the end of a presentation or event description
        // occurs, increment the counter for the sake of the
        // the next occurance of one of those, or to end up
        // with a total # of events.

        $this->curr_tag = '';

        switch ($element) {

            case 'presentation':
                $this->counter++;
                break;
            case 'special_event':
                $this->counter++;
                break;

            case 'para':
                $this->para_counter++;
                break;
        }
    }

    function get_data($parser, $data) {

        if (!$this->curr_tag) return;  // probably should note error

        if ($this->curr_tag == 'date') {
            $this->date = $data;
            return;
        }

        switch ($this->presentation[$this->counter]['type']) {

            case 'main':
            case 'kiss':
                $this->presentation[$this->counter][$this->curr_tag] = $data;
                break;
            case 'special':
                if (
                     ( ($this->curr_tag == 'para') &&
                       (array_key_exists('link',$this->presentation[$this->counter]))
                     )
                   ||
                     ( ($this->curr_tag == 'link') &&
                       (array_key_exists('para',$this->presentation[$this->counter]))
                     )
                   ) {
                    // come up with some decent error handling
                }

                if ($this->curr_tag == 'para') {
                    $this->presentation[$this->counter]['para'][$this->para_counter] .= $data;
                } else {
                    $this->presentation[$this->counter][$this->curr_tag] = $data;
                }
                break;
        }
    }

    function get_default($parser, $data) {

        switch ($data) {
            case '<![CDATA[':
                $this->curr_tag = 'para';
                break;
            default:
                $this->curr_tag = '';

        }

    }

    function read_xml($fp) {

        $xparser = xml_parser_create();
        xml_parser_set_option($xparser,XML_OPTION_CASE_FOLDING,FALSE);
        xml_set_object($xparser,&$this);
        xml_set_element_handler($xparser,'element_start','element_end');
        xml_set_character_data_handler($xparser,'get_data');
        xml_set_default_handler($xparser,'get_default');
        while ($data = fread($fp, 4096)) {
            // need a check here for UTF-8 compliance?
            if (!xml_parse($xparser,$data,feof($fp))) {
                die(sprintf( "XML error: %s at line %d\n\n",
                xml_error_string(xml_get_error_code($xparser)),
                xml_get_current_line_number($xparser)));
            }
        }
        xml_parser_free($xparser);
    }

//    function write_xml() {
//
//    }
//
    function write_html() {

        global $meeting_info;

        echo '<div class="block">';
        echo "<h3>$this->location Meeting</h3>";
        echo '<div class="content"><b>Date: </b>';
        echo $this->date . '<br><b>Location: </b>';
        printf (
            '<a href="display.php?node=%s">%s</a><p>',
            $meeting_info[$this->location]['page'],
            $meeting_info[$this->location]['loc']
        );
        //
        //  Get things sorted by time
        //
        usort($this->presentation, 'timecomp');

        foreach ($this->presentation as $present) {
            echo '<b>Time: </b>' . $present['time'];
            switch ($present['type']) {
                case 'kiss':
                case 'main':
                    echo '<p>'.$this->type_display[$present['type']];
                    //  do nifty code here to grab presentation
                    //  stuff from the appropriate XML file
                    //
                    //  for now, just some placeholder stuff
                    echo 'Title, abstract and speaker bio go here<p>';
                    break;
                case 'special':
                    echo ' <b>' . $present['title'] . '</b>';
                    foreach ($present['para'] as $paragraph) {
                        echo '<p>'. $paragraph . '</p>';
                    }
                    break;
            }
        }

        echo '</div></div>';

    }

//    function process_entry_form($form_data) {
//
//    }
//
//    function process_entry_paragraph($form_data) {
//
//    }
//
//    function file_name($date) {
//
//    }
}

//  Some functions are outside the object, because PHP
//  disallows embedding HTML sequences within objects

function write_entry_form($date, $location, $presentation) {

?>
<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
Date: <input name="date" type="text" size=16 maxlength=16><br>
Location: <select name="location" size="1">
<?php
    foreach (array('DTC','North') as $loc) {
        echo '<option value='.$loc.($loc == $location?' selected':'').'">'. $loc.'</option>';
    }
?>
</select><br>
<input type="checkbox" name="regular" value="Y">Check if regular monthly meeting<br>
Create meeting record and
<input type="radio" name="action" value="kiss">Add KISS Session<br>
<input type="radio" name="action" value="main">Add main topic<br>
<input type="radio" name="action" value="special">Add Special Event<br>
<input type="submit" value="  Create  "><br>
</form>


}

//    function write_entry_paragraph() {
//
//    }
//
?>
