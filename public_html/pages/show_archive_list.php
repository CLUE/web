<?php

// Create a dumb little associative array for purty display

$mtgnice = array('north' => 'North', 'dtc' => 'DTC');

function show_archive_list () {

// take a directory listing of presentation headers, and make a list of links
// yeah, this is a crappy way to do this.
//
// This is encapsulated as a function so that $pagedesc and $pagetitle are
// out of scope for the including presentation list

    $this_year = date ('Y', time()); // don't show future archive list, avoiding time warp

//  debug line to show all years

//  $this_year = '9999';

//  try to make an intelligent guess about the context we're running in

    $x = preg_match('/(north|dtc)/',$_SERVER['QUERY_STRING'],$bl0rp);
    if ($x) {
        // found either north or dtc, so look for those files
        $defpat = '/'.$bl0rp[0].'_[0-9]*def';
    } else {
        $defpat = '/*'; // otherwise get the whole list
    }

// get a dir list, and display the links
    
    $shellcmd = 'ls -r '._PAGE_PATH.$defpat;
    $defines = explode("\n",`$shellcmd`);
    
    //  we should have a list of the define files now
    
    foreach ($defines as $defile) {
    
        if (file_exists($defile)) {
            include ($defile);
            if ($pageyear <= $this_year) {
                $pagefile = basename($pagefile,'.html');
                echo '<li><a href="display.php?node='.$pagefile.'">'.$pagetitle.'</a></li>';
            }
        }
    }
}

?>

<div class="block">
<?php $x = preg_match('/(north|dtc)/',$_SERVER['QUERY_STRING'],$bl0rp); ?>

<h3>CLUE <?php echo $mtgnice[$bl0rp[0]]; ?> Meeting Archives</h3>
<div class="content">
<ul>
<?php show_archive_list();?>
</ul>
<p>
<a href="http://www.openoffice.org/"><img src="objects/getopenoffice_120x60_3_get.png" border=0 alt="Get Openoffice.org" align="left" height=60 width=120 hspace=5 vspace=5></a> Many of the available CLUE presentation slides are in the <a href="http://www.openoffice.org/">OpenOffice.org</a> <i>Impress</i> file format. The OpenOffice.org suite of applications is available for Linux, Solaris, Max OS X (X11), FreeBSD, and even Windows. If you need a copy, please visit the <a href="http://download.openoffice.org/">OpenOffice.org download page</a>.</p>

<p>If you are interested in giving a CLUE presentation, please contact <?php
    print ($bl0rp[0] == 'north'? NORTH: SPEAKERS);
?>. Also please read our <a href="display.php?node=speakerguide">Speaker Guidelines</a> to ensure your successful presentation.</p>
</div>
</div>
