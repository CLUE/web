<div class="block">
<h3><?php echo $pagedesc; ?></h3>
<div class="content">
CLUE would like to thank all the current and former meeting sponsors -- those individuals and companies who consistently donate prizes to give to the CLUE members.
<p>
CLUE receives many requests from Linux vendors to announce and demonstrate Linux products. The CLUE <a href="display.php?node=contacts">administrators</a> and additional CLUE members agreed on the following policies for those that wish to donate meeting prizes or demonstrate Linux products at a CLUE meeting.
<p>
The goal of the policies is to allow Linux vendors to interact with the CLUE membership while respecting the privacy rights of CLUE membership. Below are the current vendor policies.
<p>
Questions regarding the policies can be addressed to the CLUE President, Jeff Cann.
<p>
If you would like to become a meeting sponsor or show products at a vendor table, please contact the CLUE Sponsor Coordinator, <?php echo SPONSORS; ?>.
<p>
1. <b>NO PERSONAL INFORMATION:</b> CLUE will not distribute any door prizes that require exchange, registration, or collection of personal information of its members by any vendors that donate door prizes. If vendors wish to collect or exchange personal information with CLUE members, they may do so at the vendor tables (see VENDOR TABLES). This will clarify to the CLUE members that the Committee is not collecting personal information. This will also allow the CLUE members to determine individually whether they wish to register personal information with a vendor. Vendors that cannot attend a meeting to collect information for a door prize, (e.g., a month of free ISP service) should submit transferrable coupons as door prizes. The vendor coupons will be distributed as door prizes on behalf of the absentee vendor. 
<p>
2. <b>VENDOR TABLES:</b> At the monthly CLUE meeting, CLUE will provide a vendor area ("vendor tables") where any vendor can set up and distribute advertising and marketing information. Vendors may hold their own drawings, etc. at their vendor table. The vendor tables area will allow vendors to meet CLUE members, demonstrate products and services, and exchange ideas and information if desired. The availability of the vendor tables is first come, first served.
<p>
3. <b>ANNOUNCEMENT PERIOD:</b> During the announcement period at the monthly CLUE meeting (between the KISS Session and the main presentation), vendor's representatives may announce that they are in attendance representing a vendor and that CLUE members interested in the vendor's products should meet at the vendor's table. This will allow the vendors to receive advertising and marketing exposure.
<p>
4. <b>METHOD OF DRAWING:</b> The Committee will use raffle tickets for door prize drawings. The raffle tickets will have a serial number used to identify the ticket. Members interested in the door prizes may obtain a raffle ticket. After the main presentation, the drawing will be held using the raffle tickets. Serial numbers will be read to select winners. This will satisfy the Committee's concern that the door prize drawing remain anonymous.
<p>
5. <b>VENDOR ACKNOWLEDGMENT:</b> Vendors that donate prizes will be acknowledged while the prizes are given away. Given the generosity of vendors who donate door prizes, the Committee feels strongly that they should be acknowledged. Thus, the vendors that donate door prizes for CLUE meetings will be listed on the CLUE web site.
</div>
</div>
