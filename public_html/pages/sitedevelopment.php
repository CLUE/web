<div class="block">
<h3><?php echo $pagetitle; ?></h3>
<div class="content">
<h3>Introduction</h3>
One of the goals of the CLUE web site is for anyone to contribute to its
development. We would like to continue to add <a
href="#WishList">features</a> to the CLUE web site. If you have a good
idea or suggestion to improve the CLUE web site, please email the <?php print
WEBMASTER ?> for addition to the wish list. More instructions will be
added to this area. For now, you can <a href="#BuildClue">checkout</a>
the CLUE web site from our CVS repository.

<H3>CLUE Web Site Development Tools</H3> 
<ul>
<li><a href="http://www.apache.org">Apache/1.3.12 (Linux)</a> CLUE runs on Apache.</li>
<li><a href="http://www.php.net">PHP 4.0</a> used as web application framework.</li>
<li><a href="http://www.mysql.org">MySQL</a> used to store contact and biographical data for CLUE speakers, sponsors, members, and officers.</li>
<li><a href="http://www.xml.org/">XML</a> used to store CLUE meeting information.</li>
</ul>
<a name="WishList"></a>
<h3>CLUE Web Site Wish List</h3>
<ul>
<li>Dynamic updates to web site. Admin folks use HTML forms to enter gifts, sponsors, news, presentation abstracts, etc.</li>
<li>Search engine.</li>
<li>Members area -- Where CLUE members can upload and share information about Linux.</li>
</ul>
<a name="contribute"></a>
<h3>Contributing</h3>
<p>There are many ways to contribute. If you don't wish to write PHP or HTML, you can still review <a href="/site_devel/index.php">the site</a> and send suggestions to the <?php print WEBMASTER; ?>.
<p>
If you would like to contribute code, we suggest you join the <a href="<?php echo MAILSERVER.MAILMAN; ?>/clue-dev">Clue Dev</a> mailing list.
<p>
You may contribute code in two ways. One would be to download the sources, either from the <a href="#BuildClue">CVS repository</a>, or as a <a href="#tarball">tarball</a>, and send in patches. The other way would be to request CVS commit access from the <?php print WEBMASTER; ?>.
<a name="BuildClue"></a>
<h3>Anonymous CVS access to CLUE Web Site</h3>
<p>Anonymous CVS access is available for the CLUE web site code. Anonymous is a read-only CVS account (username: 'anonymous'; password: 'anonymous'. If you would like to get write permission to the CLUE web site, please drop an email to the <?php print WEBMASTER ?>. Here are the commands to download the CLUE source from our CVS server:
<ul>
<li>$ export CVSROOT=:pserver:anonymous@<?php echo _BASE_DOMAIN; ?>:/usr/local/cvsroot</li>
<li>$ cvs login</li>
<li>CVS password: [press return]</li>
<li>$ cvs checkout site_devel</li>
<li>$ cvs logout</li>
</ul>

<p>If you wish to contribute to the CVS repository, please drop an email to <?php print WEBMASTER ?>.

<h3>CVS Tips and Tricks</h3>
<UL>
	<LI>When Committing a file (that you have updated) via the command "cvs commit -m "your comments" file.ext", ensure that you cd (locally) to the directory where 
		that file is located, to avoid the error "cvs commit: nothing known about file.ext".
	</LI>
</UL>
<a name="tarball"></a>
<h3>Getting the Latest Release</h3>
The latest release of the CLUE site will be available as a gzipped tarball.
<ul><li><a href="development/site_devel.tgz">site_devel.tgz</a></li></ul>
The tarball will always coincide with the <a href="site_devel/index.php">testing area</a>. It might not contain the most recent CVS code, as that is expected to be more volatile, and possibly unstable.

<h3>Installing the code on your local machine</h3>
Both the tarball and the CVS repository contain an INSTALL file containing instructions on setting up your local machine for testing and development. You will need to have a running Apache/PHP/MySQL environment in order to test code.

<h3>Documentation</h3>
The design documents are checked in to the 'site_devel' CVS project. You need <a href="http://openoffice.org/">OpenOffice</a> to read these documents. After they are finalized, we will publish them to the website.

<p>
site_devel/docs/clue.site-design.sxw - technical explanation of site
site_devel/docs/person_schema.sxd - ERD for the person schema

<h3>CLUE Dev</h3>
Please join the <a href="<?php echo MAILSERVER.MAILMAN; ?>/clue-dev">Clue Dev</a> email
list to discuss the project! You can read the archives <a href="<?php echo MAILSERVER; ?>/pipermail/clue-dev/">here</a>.

</div>
</div>
