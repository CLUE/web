<?php

    // the code below is placeholder HTML, and will be replaced by
    // function calls for meeting information display, news items,
    // etc.

include ('functions/meeting.php');

//
//  temp code here for testing XML parser using example file
//
$xmlfile = _DATA_PATH.'/dtc/meetings/meeting-example.xml';

@$fhandle = fopen($xmlfile,'r');
if (!$fhandle) {

    error_log('Unable to open '.$xmlfile, 0);
    //  maybe e-mail error message to webmaster too?
?>
    <div class="block">

        <b>Error: <b>Unable to open <?php echo $xmlfile; ?><br>
        Please notify the <?php echo WEBMASTER; ?>.<br>
        Thanks.

    </div>

<?php
} else {

    $mtg = new meeting;
    $mtg->read_xml($fhandle);
    $mtg->write_html();

}
?>
    <div class="block">
      <h3>North Meeting</h3>
      <div class="content">
        This session, designed for the Linux community, will present an overview of
        Intel's server processor technology, roadmap, end-user benefits, and platform
        solution for 2003. The discussion includes the Intel� Xeon? processor family
        and the Intel� Itanium� processor family. This presentation will provide a
        snapshot of current open source (Linux?) technologies development on Intel
        platforms, and capabilities.
      </div>
    </div>

    <div class="block">
      <h3>CLUE News & Notes</h3>
      <div class="content">
        <ul>
        <li>O'Reilly & Associates, a CLUE premier-level sponsor, has a free 14-day trial of their online Safari Bookshelf for User Group members.</li><br>
        </ul>
Please drop a note to the webmaster if you would like to have a news item posted on the CLUE web site.
      </div>
    </div>
