<?php

// allow local (testing) install override of config file

(file_exists('../config/local_config.php'))? require ('../config/local_config.php') : require('../config/config.php');

require('./defines.php');

// some precautions here for hack attempts

$node = $_GET['node'];

// strip off leading path specification and trailing extension
$temp = explode('.',basename($node));
// force name into doc tree and wipe out any remaining klingons
$basefile = _PAGE_PATH.'/'.preg_replace('/[<>|@!$#\*~`]/','',$temp[0]).'.def';

if (file_exists($basefile)) {

    // the def file contains definitions used for rendering the page,
    // such as $pagetitle $pagedesc and $pagefile

    include($basefile);
} else {
    header('Location: ' . _NOT_FOUND);
    exit;
}

include('common.php');
include(_PAGE_PATH.'/'.$pagefile);
include('footer.php');
?>
