<?php

function admins() {
    $admins = array();
    $admins['president'] = 'President';
//    $admins['north'] = 'CLUE North';
    $admins['sponsors'] = 'Sponsor Coordinator';
    $admins['speakers'] = 'Speaker Coordinator';
//    $admins['membership'] = 'Membership Coordinator';
    $admins['events'] = 'Special Events';
    $admins['webmaster'] = 'Webmaster';
    $admins['reviews'] = 'Book Reviews';
    return $admins;
}

function admin_emails( $to ) {
    $emails = array();
    $emails['president'] = 'president@cluedenver.org';
//    $emails['north'] = 'north@cluedenver.org';
    $emails['sponsors'] = 'sponsors@cluedenver.org';
    $emails['speakers'] = 'speakers@cluedenver.org';
//    $emails['membership'] = 'membership@cluedenver.org';
    $emails['events'] = 'events@cluedenver.org';
    $emails['webmaster'] = 'thewebmasterguy@cluedenver.org';
    $emails['reviews'] = 'reviews@cluedenver.org';
    return $emails[$to];
}

function processForm() {
    global $name, $from, $subject, $message, $to;

    $now =  date ( "D M j Y h:i a", time() );
    $subject = "CLUE Web Site Contact Form - $subject ($now)";

    if ( mail( admin_emails($to), stripslashes($subject), stripslashes($message)."\n\nIP Address: ".$_SERVER['REMOTE_ADDR'], 
               "From: $name <$from>" ) ) {
        showResult( 'successful' );
    } else {
        showResult( 'unsuccessful' );
    }
}

function showForm() {
    global $PHP_SELF, $to, $name, $from, $subject, $message;

// generate a super simple "turing test" CAPTCHA image

    $tuname = array (
        'happy.gif',
        'grumpy.gif',
        'sleepy.gif',
        'doc.gif',
        'bashful.gif',
        'dopey.gif',
        'sneezey.gif',
        'bert.gif',
        'ernie.gif',
        'alan.gif'
    );

    $tstring = '';
    $captcha = '<nobr>';
    for ($i = 1; $i <= 5; $i++) {
        $n = rand(0, 9);
        $tstring .= $n;
        $captcha .= '<img src="objects/alan/';
        $captcha .= $tuname[$n];
        $captcha .= '" />';
    }
    $captcha .= '</nobr>';

    $alanuser = '';

    $form = new HTML_Form( $PHP_SELF, 'POST' );
    $form->addText( 'name', 'Name ', $name, 40 );
    $form->addText( 'from' ,'Email ', $from, 40 );
    $form->addText( 'subject', 'Subject ', $subject, 40 );
    $form->addSelect( 'to', 'To ', admins(), $to ) ;
    $form->addTextarea( 'message', 'Message ', $message, 40, 10 );
    $form->addPlaintext ('Are you human?','If so, please enter the following number in the "Turing test" field when submitting your message.<br>'.$captcha);
    $form->addText ('alanuser', 'Turing test', $alanuser, 5);
    $form->addSubmit( 'submit', 'Submit' );
    $form->addHidden( 'action', 'WorkOrder' );
    $form->addHidden ('alancheck', md5($tstring._FLUFFER));
//  next field just for testing porpoises
    $form->addHidden('thenumber', $tstring);

    $form->display();
}

function showResult( $result ) {
    echo 'Your email was '.$result.'.  Thanks for your time!';
}

function validateForm() {
    global $name, $from, $subject, $message, $submit, $alancheck, $alanuser;
    $errors = array ();

    if (trim($submit) != 'Submit') {
        // This appears to work fine, and doesn't seem to be any
        // current value in logging the info
        //
        // log_bot('bot detection: submit value incorrect');
?>
<h3>Bot detection triggered</h3>
IP Address <?php echo $_SERVER['REMOTE_ADDR']; ?> logged
<?php
        exit;
    }

    if ( !$name ) {
    array_push( $errors, 'Name' );
    }

    if ( !$from ) {
    array_push( $errors, 'Email' );
    } else {
        if (!ereg( '^[A-Za-z0-9._-]+\@([A-Za-z0-9_-]+\.)+[A-Za-z]+$', $from )) {
            array_push( $errors, "Email ($from) is not in required format." );
            array_push( $errors, "Email format: <samp>user@domain.com</samp>" );
        }
    }

    if ( !$subject ) array_push( $errors, 'Subject' );

    if ( !$message ) array_push( $errors, 'Message' );

    if (!$alanuser) {
        array_push ($errors, 'Turing test');
    } else {
        if (md5($alanuser._FLUFFER) != $alancheck) array_push ($errors, 'Turing test');
    }

    $err = count( $errors );
    if ( $err ) {

        echo '<div class="error">';
        if ( $err == 1 ) { $fields = 'field'; } else { $fields = 'fields'; }
        echo '<b>Error &#151; </b>';
        echo "<em>The form is missing the following $fields:<br></em>";

        while ( each( $errors ) ) {
            echo array_shift( $errors ) . '<br>';
        }
        echo '</div>';
        showForm();
    } else {
        return true;
    }
}

function log_bot($imessage) {

// function to generate notfication e-mail if a bot is detected
// or can be used generically, actually. probably should just move
// it to a generic func. lib -- later, when I feel like being good.

//  snag the raw POST data
    $data = file_get_contents('php://input');

    $tmpfname = tempnam("/tmp", "contactform_");
    $handle = fopen($tmpfname, "w");
    fwrite($handle, "POST data:\n\n");
    fwrite($handle, $data);
    fwrite($handle, "\n\nGET Data:\n\n");
    while (list($postkey, $postval) = each($_GET)) {
        fwrite($handle, $postkey.":".$postval."\n");
    }
    fclose($handle);

    $recip = admin_emails('webmaster');
    $imessage .= "\n\nLogged IP: ".$_SERVER['REMOTE_ADDR'];
    $imessage .= "\n\nSaved form data: ".$tmpfname;
    if (mail($recip, 'CLUE webserver log message', $imessage)) return true;
    error_log('contactform.php unable to send e-mail for log_bot');
    exit;
}

function logformtest() {
    global $name,$from,$subject,$to,$message,$alanuser,$alancheck,$thenumber;

    $ptologfile = fopen('/tmp/contactlog.txt', 'a') or die ("Can't open logfile");
    $junk = sprintf("\nName:%s\nFrom:%s\nSubject:%s\nTo:%s\nMessage:%s\nalanuser:%s\nthenumber:%s\nalancheck:%s\nfluffer:%s\n", $name,$from,$subject,$to,$message,$alanuser,$thenumber,$alancheck,_FLUFFER);
    fwrite($ptologfile, $junk) or die ("Can't write to log file");
    fclose($ptologfile);
}

// allow local (testing) install override of config file

(file_exists('../config/local_config.php'))? require ('../config/local_config.php') : require('../config/config.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  while (list($postkey, $postval) = each($_POST)) {
    $$postkey = htmlspecialchars($postval);
  }
}else{
  while (list($postkey, $postval) = each($_GET)) {
    $$postkey = htmlspecialchars($postval);
  }
}

require('./defines.php');

require_once( 'HTML/Form.php' );

$pagetitle = 'Clue Contact Form';
$pagedesc = $pagetitle;
include('common.php');

echo '<div class="block"><h3>Contact a CLUE Volunteer</h3><div class="content">';

if ( isset( $submit ) ) {

    // the contact form spam seems to be handled properly now
    // so commenting this out unless needed again
    //
    // logformtest();

    if ( validateForm() ) processForm();
} else {

    showForm();
}

echo '</div></div>';
include('footer.php');

?>
