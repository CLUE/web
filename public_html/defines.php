<?php
//  Global Defines
define( "MAILSERVER", _BASE_URL );
define( "MAILMAN", MAILSERVER."/mailman/listinfo" );

//  Volunteer Email Addresses
define( "PRESIDENT",'<a href="contactform.php?to=president">Dave Anselmi</a>' );
// define( "NORTH",'<a href="contactform.php?to=north">Zach Giezen</a>' );
define( "SPONSORS",'<a href="contactform.php?to=sponsors">Dave Anselmi</a>' );
define( "SPEAKERS",'<a href="contactform.php?to=speakers">Dave Anselmi</a>' );
// define( "MEMBERSHIP",'<a href="contactform.php?to=membership">Collins Richey</A>');
define( "EVENTS",'<a href="contactform.php?to=events">Dave Anselmi</a>' );
define( "WEBMASTER",'<a href="contactform.php?to=webmaster">webmaster</A>' );
define( "WEBMASTER_P",'<a href="contactform.php?to=webmaster">Vacant</A>' );
define( "REVIEWS",'<a href="contactform.php?to=reviews">CLUE Reviews</a>' );

define ('_FLUFFER', 'vi is the editor of choice for true geeks');

//  Useful arrays

$meeting_info = array (
    'DTC' => array (
        'loc'  => 'Oracle Building 2 DTC',
        'page' => 'dtc_info'
    ),
    'NORTH' => array (
        'loc'  => 'DeVry University',
        'page' => 'north_info'
    )
);

?>
