#!/bin/bash
#
#   A shell script to publish the CLUE website from CVS (updated to use git)
#
#   Author: Jed S. Baer
#   Date:   16-Dec-2004
#
#   In order to do this, and also accomplish getting rid
#   of directories which have been removed, the website
#   will be published to a new directory each time, and
#   the document root symbolic link will be redirected to it.
#
#   The symlink scheme makes it possible to completely update
#   the site without having 404s because files are temporarily
#   unavailable.
#
#   Now required to run from a git tree.  Set WEBROOT before running to override
#   the default location.

# for debugging, redirect stdout
exec 2>&1
# for even more debugging set trace
#set -x

echo 'Setup phase'

# set HOME, since Apache is running as apache only in the sense
# of effective UID/GUID, not in environment
export HOME=/usr/local/www

WEBROOT=${WEBROOT:-/usr/local/www}
WEBDIR=$WEBROOT/clue;

OLDDIR=`readlink $WEBDIR`
NEWDIR=`mktemp -d $WEBROOT/clue_XXXXXX`

echo 'git Export phase'

git archive master | tar -x -C $NEWDIR

# Now the fun part, move the html link to the new directory
# and delete the old directory

echo 'Moving symlink'

ln -snf $NEWDIR $WEBDIR || {
  echo -e "\nUnable to link to new web directory. Aborting"
  exit
}
# if this is a first-time run, there isn't an OLDDIR
[[ -n $OLDDIR ]] && rm -rf $OLDDIR

echo -e "\nPublishing complete"

echo -e "\nCopying publishing scripts to cgi-bin directory"

chmod 600 $WEBROOT/.htpasswd
chmod 600 ${WEBROOT}/${WEBDIR}/public_html/admin/.htaccess

#dumb little line for breadcrumb
